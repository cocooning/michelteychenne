# michelteychenne

## Cluster

### manager(s)
#### master1 (192.168.1.103)
- Version docker : 26.0.1
- Orangepi 4B
- eMMC 16 Go
- SSD 32 Go (USB2)

### worker(s)
#### worker1 (192.168.1.102)
- Version docker : 26.0.1
- Raspberry 3B+
- SD card 16 Go

## Apps
### master1
- haproxy
- hassio (postgres)
- postgres
- zigbee (A déplacer sur worker1)
### worker1
- mosquitto